<?php
    namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller{
    /**
     * @route("/")
     */
    public function index(){
        return $this->render('main/index.html.twig');
        }
}