# Task for Fixture


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

```
VirtualBox (v6.0)
Vagrant (v2.2.3)
```

### Installing



*  Step 1:

    Download Zip



*  Step 2:

    Extract folder "Task" on **Desktop**



*  Step 3:

    Go to C:\Windows\System32\drivers\etc, open "hosts" and add the next line at the end of the file:
    ```
    192.168.10.10 fixture.test
    ```
*Due to the administrator's restriction, you can temporarily move the file to the desktop, edit it there and move it back to its original folder*

*  Step 4:

    Open a new terminal and use the following commands:

    ```
    # cd Desktop
    # cd Task
    # cd Homestead
    # vagrant up
    
    ```
    
    Wait for Vagrant to finish downloading the files you need and configuring the virtual machine

*  Step 5:

    Open a browser and go to fixture.test



## Built With

* [Symfony 4](https://symfony.com/) - The web framework used
* [Laravel/Homestead](https://laravel.com/docs/5.7/homestead) - Vagrant box



## Authors

* **Rodrigo Correa** 


